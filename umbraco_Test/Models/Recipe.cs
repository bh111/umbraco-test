﻿using Newtonsoft.Json;

namespace UmbracoTest.Models
{
	public class Recipe
	{
		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("description")]
		public string Description { get; set; }

		[JsonProperty("imageId")]
		public int ImageId { get; set; }
	}
}