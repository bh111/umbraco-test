﻿using System.Linq;
using UmbracoTest.Models;

namespace UmbracoTest.DataAccess
{
	public class RecipeDataLoader
	{
		private RecipeContext db = new RecipeContext();

		public Recipe[] GetRecipes()
		{
			return db.Recipes.ToArray();
		}

		public Recipe GetRecipe(int recipeId)
		{
			return db.Recipes.Find(recipeId);
		}

		public void CreateRecipe(Recipe recipe)
		{
			if (recipe == null)
			{
				return;
			}

			db.Recipes.Add(recipe);
			db.SaveChanges();
		}

		public void UpdateRecipe(Recipe recipe)
		{
			if (recipe == null)
			{
				return;
			}

			var entity = db.Recipes.Find(recipe.Id);
			if (entity != null)
			{
				db.Entry(entity).CurrentValues.SetValues(recipe);
				db.SaveChanges();
			}
		}

		public void RemoveRecipe(int recipeId)
		{
			var entity = db.Recipes.Find(recipeId);
			if (entity != null)
			{
				db.Recipes.Remove(entity);
				db.SaveChanges();
			}
		}
	}
}