﻿using umbraco.interfaces;

namespace UmbracoTest.CustomCode.Actions
{
	public class AddRecipeAction : IAction
	{
		public char Letter
		{
			get { return default(char); }
		}

		public bool ShowInNotifier
		{
			get { return false; }
		}

		public bool CanBePermissionAssigned
		{
			get { return false; }
		}

		public string Icon
		{
			get { return ""; }
		}

		public string Alias
		{
			get { return "add"; }
		}

		public string JsFunctionName
		{
			get { return ""; }
		}

		public string JsSource
		{
			get { return ""; }
		}
	}
}