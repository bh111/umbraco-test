﻿angular.module('umbraco').controller('RecipeTree.AddRecipe.Controller', function ($scope, $routeParams, $http, $location, dialogService, navigationService) {
	$scope.recipe = {};
	$scope.imageFile = null;
	$scope.lock = false;

	$scope.update = function () {
		$scope.lock = true;
		$http.post('/umbraco/backoffice/api/RecipeApi/CreateRecipe', $scope.recipe).success(function () {
			$scope.lock = false;
			navigationService.syncTree({ tree: 'recipesTree', path: ["-1", "123d"], forceReload: true });
			$location.path("/recipessection/recipesTree/edit/Recipes");
		}).error(function () {
			$scope.lock = false;
		});
	}

	$scope.openMediaPicker = function () {
		dialogService.mediaPicker({ callback: populatePicture });
	}

	$scope.removePicture = function () {
		$scope.imageFile = null;
		$scope.recipe.imageId = 0;
	}

	function populatePicture(item) {
		$scope.imageFile = item;
		$scope.recipe.imageId = item.id;
	}
})
.directive('editTemplate', function () {
	return {
		templateUrl: function () {
			return '/App_Plugins/RecipeTree/Views/Template.html';
		}
	};
});