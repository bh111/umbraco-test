﻿angular.module('umbraco').controller('RecipeTree.RemoveRecipe.Controller', function ($scope, $routeParams, $http, $location, navigationService) {
	$scope.remove = function () {
		$scope.lock = true;
		$http.get('/umbraco/backoffice/api/RecipeApi/RemoveRecipe?recipeId=' + $routeParams.id).then(function () {
			$scope.lock = false;
			navigationService.syncTree({ tree: 'recipesTree', path: ["-1", "123d"], forceReload: true });
			$location.path("/recipessection/recipesTree/edit/Recipes");
		});
	}
});