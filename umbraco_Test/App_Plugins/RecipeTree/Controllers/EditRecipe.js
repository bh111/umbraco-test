﻿angular.module('umbraco').controller('RecipeTree.EditRecipe.Controller', function ($scope, $routeParams, $http, dialogService, entityResource, mediaHelper) {
	$scope.recipe = null;
	$scope.imageFile = null;
	$scope.lock = false;

	$http.get('/umbraco/backoffice/api/RecipeApi/GetRecipe?recipeId=' + $routeParams.id).success(function (response) {
		$scope.recipe = response;
		if ($scope.recipe.imageId > 0) {
			entityResource.getById($scope.recipe.imageId, "Media").then(function (item) {
				$scope.recipe.imageId = item.id;
				$scope.imageFile = item;
				$scope.imageFile.image = mediaHelper.resolveFileFromEntity(item);
			});
		}
	});

	$scope.update = function () {
		$scope.lock = true;
		$http.post('/umbraco/backoffice/api/RecipeApi/UpdateRecipe', $scope.recipe).then(function () {
			$scope.lock = false;
		});
	}

	$scope.openMediaPicker = function () {
		dialogService.mediaPicker({ callback: populatePicture });
	}

	$scope.removePicture = function () {
		$scope.imageFile = null;
		$scope.recipe.imageId = 0;
	}

	function populatePicture(item) {
		$scope.imageFile = item;
		$scope.recipe.imageId = item.id;
	}
})
.directive('editTemplate', function () {
	return {
		templateUrl: function () {
			return '/App_Plugins/RecipeTree/Views/Template.html';
		}
	};
});