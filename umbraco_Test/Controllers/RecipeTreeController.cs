﻿using System.Net.Http.Formatting;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;
using UmbracoTest.DataAccess;
using UmbracoTest.Models;

namespace UmbracoTest.Controllers
{
	[Tree("recipessection", "recipesTree", "Recipes")]
	[PluginController("RecipeTree")]
	public class RecipeTreeController : TreeController
	{
		protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
		{
			TreeNodeCollection nodes = new TreeNodeCollection();
			RecipeDataLoader loader = new RecipeDataLoader();

			if (id == "-1")
			{
				nodes.Add(CreateTreeNode("0", id, queryStrings, "Recipes", "icon-food", true));
			}
			else
			{
				Recipe[] recipes = loader.GetRecipes();
				foreach (Recipe recipe in recipes)
				{
					nodes.Add(CreateTreeNode(recipe.Id.ToString(), id, queryStrings, recipe.Name, "icon-calendar-alt"));
				}
			}
			return nodes;
		}
		protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
		{
			var collection = new MenuItemCollection();
			if (id == "0")
			{
				MenuItem menuitem = new MenuItem("createrecipe", "Create recipe");
				menuitem.Icon = "icon-document";
				menuitem.NavigateToRoute("/recipessection/recipesTree/add/0");
				collection.Items.Add(menuitem);
			}
			else
			{
				MenuItem menuitem = new MenuItem("deleterecipe", "Delete recipe");
				menuitem.Icon = "icon-delete";
				menuitem.NavigateToRoute("/recipessection/recipesTree/remove/" + id);
				collection.Items.Add(menuitem);
			}
			return collection;
		}
	}
}