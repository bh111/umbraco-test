﻿using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using Umbraco.Web.WebApi;
using UmbracoTest.DataAccess;
using UmbracoTest.Models;

namespace UmbracoTest.Controllers
{
	public class RecipeApiController : UmbracoAuthorizedApiController
	{
		[AcceptVerbs("GET")]
		[UmbracoAuthorize]
		public HttpResponseMessage GetRecipe(int recipeId)
		{
			RecipeDataLoader loader = new RecipeDataLoader();
			Recipe recipe = loader.GetRecipe(recipeId);
			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
			if (recipe != null)
			{
				string data = JsonConvert.SerializeObject(recipe);
				response.Content = new StringContent(data, Encoding.UTF8, "application/json");
			}
			return response;
		}

		[AcceptVerbs("GET")]
		[UmbracoAuthorize]
		public HttpResponseMessage RemoveRecipe(int recipeId)
		{
			HttpRequest httpRequest = HttpContext.Current.Request;
			RecipeDataLoader loader = new RecipeDataLoader();
			loader.RemoveRecipe(recipeId);
			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
			return response;
		}

		[AcceptVerbs("POST")]
		[UmbracoAuthorize]
		public HttpResponseMessage UpdateRecipe(Recipe recipe)
		{
			HttpRequest httpRequest = HttpContext.Current.Request;
			RecipeDataLoader loader = new RecipeDataLoader();
			loader.UpdateRecipe(recipe);
			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
			return response;
		}

		[AcceptVerbs("POST")]
		[UmbracoAuthorize]
		public HttpResponseMessage CreateRecipe(Recipe recipe)
		{
			HttpRequest httpRequest = HttpContext.Current.Request;
			RecipeDataLoader loader = new RecipeDataLoader();
			loader.CreateRecipe(recipe);
			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
			return response;
		}
	}
}